package com.android.demo.arbreGenial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jibe on 06/01/14.
 */
public class Relation extends Activity {
    private int mNoteNumber = 1;
    private NotesDbAdapter mDbHelper;
    private Spinner listeRelations,listeAllPersonnes;
    private Button buttonSaveRelation;
    private long idPersonne;
    private ArrayList<String> personneIdArray = new ArrayList<String>();
    public Context mContext;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        setContentView(R.layout.relation_layout);
        final Intent intent = getIntent();
        mDbHelper = new NotesDbAdapter(this);
        mDbHelper.open();
        idPersonne = intent.getLongExtra("person_id",0);
        Cursor personne = mDbHelper.fetchNote(idPersonne);
        if(personne != null){
            String name =  personne.getString(1);
            TextView txtv_name = (TextView) findViewById(R.id.textView);
           txtv_name.setText(name);
        }

        // Remplissage du spinner de relations
        listeRelations = (Spinner) findViewById(R.id.spinnerRelation);
        List<String> relations = new ArrayList<String>();
        relations.add("parent de");
        relations.add("enfant de");
        relations.add("conjoint de");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, relations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listeRelations.setAdapter(adapter);

        //remplissage du spinner des autres personnes
        Cursor allPersonnes = mDbHelper.fetchAllNotes();
        startManagingCursor(allPersonnes);
        String[] from = new String[] { NotesDbAdapter.KEY_NAME };
        int[] to = new int[] { R.id.text1 };
        SimpleCursorAdapter adapterAllPersonne =
                new SimpleCursorAdapter(this, R.layout.notes_row, allPersonnes, from, to);
        listeAllPersonnes = (Spinner) this.findViewById(R.id.spinnerPersonneReliee);
        listeAllPersonnes.setAdapter(adapterAllPersonne);
        allPersonnes.moveToFirst();
        int index = allPersonnes.getColumnIndexOrThrow(mDbHelper.KEY_ROWID);
        do {
            String id = allPersonnes.getString(index);
            personneIdArray.add(id);
            allPersonnes.moveToNext();
        }while(!allPersonnes.isLast());
        if(allPersonnes.isLast()){
            String id = allPersonnes.getString(index);
            personneIdArray.add(id);
        }
        // Si on vient de l'activité update, on selectionne les items de la relation
        if(intent.hasExtra("update")){
            long idRelation = intent.getLongExtra("id_relation",0);
            Cursor relationCourante = mDbHelper.getCurrentRelation(idRelation);
            relationCourante.moveToFirst();
            listeRelations.setSelection(Integer.parseInt(relationCourante.getString(relationCourante.getColumnIndexOrThrow(mDbHelper.KEY_TYPE))));
            String id = relationCourante.getString(relationCourante.getColumnIndexOrThrow(mDbHelper.KEY_ID2));
            int position = personneIdArray.indexOf(id);
            listeAllPersonnes.setSelection(position);

        }
        buttonSaveRelation = (Button) findViewById(R.id.buttonSaveRelation);

        buttonSaveRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long lienRelation = listeRelations.getSelectedItemId();
                long lienPersonne = listeAllPersonnes.getSelectedItemId();

                if(idPersonne != lienPersonne){
                    if(intent.hasExtra("update")){
                        long idRelation = intent.getLongExtra("id_relation",0);
                        mDbHelper.updateRelation(idRelation, idPersonne, lienPersonne, lienRelation);
                        Toast.makeText(mContext,"Relation mise à jour", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if(!mDbHelper.existeRelation(idPersonne,lienPersonne)){
                            mDbHelper.addRelation(idPersonne,lienPersonne,lienRelation);
                            Toast.makeText(mContext,"Relation ajoutée", Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(mContext,"Une relation entre ces deux personnes existe déjà", Toast.LENGTH_LONG).show();
                        }
                    }
                    Intent intentVisuRelation = new Intent(mContext,ViewRelations.class);
                    intentVisuRelation.putExtra("person_id", idPersonne);
                    intentVisuRelation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentVisuRelation);
                }
                else{
                    Toast.makeText(mContext,"Vous ne pouvez pas ajouter une relation sur la même personne", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}