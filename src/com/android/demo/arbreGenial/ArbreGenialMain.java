/*
 * Copyright (C) 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.demo.arbreGenial;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ArbreGenialMain extends ListActivity {
    public static final int INSERT_ID = Menu.FIRST;
    public static final int DELETE_ID = Menu.FIRST+1;
    public static final int IMPORT_ID = Menu.FIRST+2;
    public static final int IMPORT_ALL_ID = Menu.FIRST+3;
    public static final int ADD_RELATION = Menu.FIRST+4;
    public static final int DELETE_ALL = Menu.FIRST+5;
    public static final int PICK_CONTACT_REQUEST = 1;
    private int mNoteNumber = 1;
    private NotesDbAdapter mDbHelper;
    private Context mContext;
    ProgressBar progressBar;
    TextView txt_percentage;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notepad_list);
        mContext = getApplicationContext();
        mDbHelper = new NotesDbAdapter(this);
        mDbHelper.open();
        fillData();
        registerForContextMenu(getListView());

        ListView lv = getListView();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // When clicked, show a toast with the TextView text
                Intent intentVisuRelation = new Intent(getApplicationContext(),ViewRelations.class);
                intentVisuRelation.putExtra("person_id",parent.getItemIdAtPosition(position));
                startActivity(intentVisuRelation);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*boolean result = super.onCreateOptionsMenu(menu);
        menu.add(1, IMPORT_ID ,0, R.string.menu_import).setIcon(R.drawable.add);
        menu.add(1, IMPORT_ALL_ID, 0 , R.string.menu_import_all).setIcon(R.drawable.addgroup);
        menu.add(1,DELETE_ALL,0,R.string.menu_delete_all).setIcon(R.drawable.deletegroup);*/
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Allow to delete an entry with the ContextMenu
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);
        menu.add(1,ADD_RELATION,0,R.string.menu_add_relation);
        menu.add(0, DELETE_ID, 0, R.string.menu_delete);
    }


    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case ADD_RELATION:
                Intent intentRelation = new Intent(ArbreGenialMain.this, Relation.class);
                AdapterContextMenuInfo personSelected = (AdapterContextMenuInfo) item.getMenuInfo();
                intentRelation.putExtra("person_id",personSelected.id);
                startActivity(intentRelation);
                return true;
            case DELETE_ID:
                AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
                mDbHelper.deleteNote(info.id);
                fillData();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                pickContact();
                return true;
            case R.id.action_add_group:
                importAllContact();
                return true;
            case R.id.action_remove_all:
                removeAllContact();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Vérifie quelle requete est en train de répondre
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Vérifie que la requete est ok
            if (resultCode == RESULT_OK) {
                // L'utilisateur prend un contact.
                // The Intent's data Uri identifies which contact was selected.
                Uri contactUri = data.getData();
                String[] projection = {Phone.DISPLAY_NAME};
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                int column = cursor.getColumnIndex(Phone.DISPLAY_NAME);
                String name = cursor.getString(column);
                // Do something with the contact here (bigger example below)
                long res = mDbHelper.createNote(name);
                if(res == -1){
                    Toast.makeText(mContext,"Cette personne est déja dans la liste", Toast.LENGTH_SHORT).show();
                }
                else {
                   Toast.makeText(mContext,"Personne ajoutée", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public void createNote(String name) {
        mDbHelper.createNote(name);
    }

    public void fillData() { //AFFICHAGE
        // Get all of the notes from the database and create the item list
        Cursor c = mDbHelper.fetchAllNotes();
        startManagingCursor(c);

        String[] from = new String[] { NotesDbAdapter.KEY_NAME };
        int[] to = new int[] { R.id.text1 };

        // Now create an array adapter and set it to display using our row
        SimpleCursorAdapter notes =
                new SimpleCursorAdapter(this, R.layout.notes_row, c, from, to);
        setListAdapter(notes);
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }

    private void importAllContact(){
        //progressBar =  (ProgressBar) findViewById(R.id.progress);
        //txt_percentage= (TextView) findViewById(R.id.txt_percentage);

        importAllContactTask importAsync = new importAllContactTask(this);
        importAsync.execute(this);
    }

    private void removeAllContact(){
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            //.setTitle("Closing Activity")
            .setMessage("Etes-vous sûr de vouloir supprimer tous les contacts ?")
            .setPositiveButton("Oui", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mDbHelper.deleteAllNotes();
                    fillData();
                }

            })
            .setNegativeButton("Non", null)
            .show();
    }

}
