package com.android.demo.arbreGenial;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.widget.Toast;

/**
 * Created by Jibe on 06/01/14.
 */

public class importAllContactTask extends AsyncTask<ArbreGenialMain, Integer, Boolean> {

    private ArbreGenialMain mActivity = null;
    private ArbreGenialMain activity = null;
    private ProgressDialog progDailog = null;

    public importAllContactTask(ArbreGenialMain activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progDailog = new ProgressDialog(activity);
        progDailog.setMessage("Chargement...");
        progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDailog.setCancelable(true);
        progDailog.show();
    }

    @Override
    protected Boolean doInBackground(ArbreGenialMain... arg0) {
        Boolean bool;
        mActivity = arg0[0];
        String[] mProjection =
                {
                        ContactsContract.Contacts.DISPLAY_NAME
                };

        String mSelectionClause = Contacts.HAS_PHONE_NUMBER;

        Cursor mCursor = mActivity.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                mProjection, mSelectionClause, null, null);
        // Some providers return null if an error occurs, others throw an exception
        if (null == mCursor) {
            bool = false;
        }
        else
        if (mCursor.getCount() < 1) {
            bool = false;
        }
        else {
            int index = mCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            while (mCursor.moveToNext()) {
                String name = mCursor.getString(index);
                mActivity.createNote(name);
            }
            bool = true;
        }
        return bool;
    }

    protected void onPostExecute (Boolean result) {
        if(result){
            mActivity.fillData();
            Toast.makeText(mActivity, "Importation terminée !", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(mActivity, "Echec de l'importation", Toast.LENGTH_SHORT).show();

        progDailog.hide();
    }
}
