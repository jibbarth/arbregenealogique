package com.android.demo.arbreGenial;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jibe on 10/01/14.
 */
public class ViewRelations extends Activity {

    private NotesDbAdapter mDbHelper;
    private Context mContext;
    private long idPersonne;
    private Button buttonAddRelation ;
    private ArrayList<String> relationIdArray = new ArrayList<String>();
    public static final int DELETE_RELATION = Menu.FIRST;
    public static final int UPDATE_RELATION = Menu.FIRST+1;
    private ActionBar supportActionBar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_relation);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        mContext = getApplicationContext();
        mDbHelper = new NotesDbAdapter(this);
        mDbHelper.open();
        idPersonne = intent.getLongExtra("person_id", 0);

        Cursor personne = mDbHelper.fetchNote(idPersonne);
        if (personne != null) {
            String name = personne.getString(1);
            TextView txtv_name = (TextView) findViewById(R.id.textViewName);
            txtv_name.setText(name);
        }

        // Get all relation
        afficheRelations();
        registerForContextMenu((ListView) findViewById(R.id.listView));

        buttonAddRelation = (Button) findViewById(R.id.buttonAddRelation);

        buttonAddRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intentAddRelation = new Intent(getApplicationContext(), Relation.class);
                intentAddRelation.putExtra("person_id", idPersonne);
                startActivityForResult(intentAddRelation, 1);
            }
        });

    }

    /**
     * Allow to delete an entry with the ContextMenu
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);

        menu.add(1, UPDATE_RELATION, 0, R.string.menu_update_relation);
        menu.add(0, DELETE_RELATION, 0, R.string.menu_delete_relation);

    }


    public boolean onContextItemSelected(MenuItem item) {
        int positionRelation;
        long selectionID;
        AdapterView.AdapterContextMenuInfo info;
        switch(item.getItemId()) {
            case UPDATE_RELATION:
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                positionRelation = (int)info.id;
                selectionID = Long.parseLong(relationIdArray.get(positionRelation));
                Intent intentUpdateRelation = new Intent(getApplicationContext(),Relation.class);
                intentUpdateRelation.putExtra("person_id",idPersonne);
                intentUpdateRelation.putExtra("update", true);
                intentUpdateRelation.putExtra("id_relation", selectionID);
                intentUpdateRelation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentUpdateRelation);
                return true;
            case DELETE_RELATION:
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                positionRelation = (int)info.id;
                selectionID = Long.parseLong(relationIdArray.get(positionRelation));
                mDbHelper.deleteRelation(selectionID);
                Toast.makeText(mContext,"Relation deleted", Toast.LENGTH_LONG).show();
                afficheRelations();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    public void afficheRelations(){
        ListView listView = (ListView) findViewById(R.id.listView);
        relationIdArray.clear();
        Cursor listeRelations = mDbHelper.getRelations(idPersonne);
        List<String> relations = new ArrayList<String>();
        while(listeRelations.moveToNext()){
            String idRelation =(listeRelations.getString(listeRelations.getColumnIndexOrThrow(mDbHelper.KEY_ROWID)));
            long id2 = Long.parseLong(listeRelations.getString(listeRelations.getColumnIndexOrThrow(mDbHelper.KEY_ID2)));
            long type = Long.parseLong(listeRelations.getString(listeRelations.getColumnIndexOrThrow(mDbHelper.KEY_TYPE)));
            String type_label = null ;
            switch ((int)type){
                case 0 : type_label = "Parent de :";
                    break;
                case 1 : type_label = "Enfant de :";
                    break;
                case 2 : type_label = "Conjoint de :";
                    break;
            }
            Cursor personneReliee = mDbHelper.fetchNote(id2);
            String namePersonneReliee = personneReliee.getString(1);
            relationIdArray.add(idRelation);
            relations.add(type_label +" "+ namePersonneReliee);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, relations);
        listView.setAdapter(adapter);
    }
}