/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.android.demo.arbreGenial;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations
 * for the notepad example, and gives the ability to list all notes as well as
 * retrieve or modify a specific note.
 * 
 * This has been improved from the first version of this tutorial through the
 * addition of better error handling and also using returning a Cursor instead
 * of using a collection of inner classes (which is less scalable and not
 * recommended).
 */
public class NotesDbAdapter {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "name";

    public static final String KEY_ID1 = "_id1";
    public static final String KEY_ID2 = "_id2";
    public static final String KEY_TYPE = "type";

    private static final String TAG = "NotesDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_CREATE =
        "create table persons (_id integer primary key autoincrement, "
        + "name text not null);";

    private static  final String DATABASE_CREATE_RELATION =
            "create table relations (_id integer primary key, _id1 integer, _id2 integer, type integer);";

    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "persons";
    private static final String DATABASE_TABLE_RELATION = "relations";

    private static final int DATABASE_VERSION = 8;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE_RELATION);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS persons");
            db.execSQL("DROP TABLE IF EXISTS relations");
            onCreate(db);
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     * 
     * @param ctx the Context within which to work
     */
    public NotesDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     * 
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public NotesDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }


    /**
     * Create a new note using the title and body provided. If the note is
     * successfully created return the new rowId for that note, otherwise return
     * a -1 to indicate failure.
     *
     * @param name the name of the person
     * @return rowId or -1 if failed
     */
    public long createNote(String name) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        if(!checkExistencePerson(name)){
            return mDb.insert(DATABASE_TABLE, null, initialValues);
        }
        else return -1;
    }

    /**
     * Methode qui check si le nom existe déja dans la bd, pour éviter les doublons
     * @param  name
     * @return true si il y est, false sinon
     */
    public boolean checkExistencePerson(String name){
        Cursor mCursor = mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME}, KEY_NAME + " like  \"" + name +"\"" , null, null, null,null);
        if (null == mCursor || mCursor.getCount()  <1) {
            return false;
        }
        else{
            return true;
        }
    }
    /**
     * Delete the note with the given rowId
     * 
     * @param rowId id of note to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteNote(long rowId) {
        // Remove all relation for this people

        mDb.delete(DATABASE_TABLE_RELATION, KEY_ID1 + "=" + rowId, null);
        mDb.delete(DATABASE_TABLE_RELATION, KEY_ID2 + "=" + rowId, null);

        return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    /**
     * Delete all notes
     *
     * @return true if deleted, false otherwise
     */
    public boolean deleteAllNotes() {

        return mDb.delete(DATABASE_TABLE, null, null) > 0;
    }

    /**
     * Return a Cursor over the list of all notes in the database
     * 
     * @return Cursor over all notes
     */
    public Cursor fetchAllNotes() {

        return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME}, null, null, null, null, null);
    }

    /**
     * Return a Cursor positioned at the note that matches the given rowId
     * 
     * @param rowId id of note to retrieve
     * @return Cursor positioned to matching note, if found
     */
    public Cursor fetchNote(long rowId) {

        Cursor mCursor = mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME}, KEY_ROWID + "=" + rowId, null, null, null,null);

        if (mCursor != null) {
            mCursor.moveToFirst();
            return mCursor;
        }else
            return null;

    }

    /**
     * Update the note using the details provided. The note to be updated is
     * specified using the rowId, and it is altered to use the title and body
     * values passed in
     * 
     * @param rowId id of note to update
     * @param name value to set note title to
     * @return true if the note was successfully updated, false otherwise
     */
    public boolean updateNote(long rowId, String name) {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
       

        return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

    /**
     * Methode pour ajouter une relation
     */

    public long addRelation(long id1, long id2, long type){
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_ID1, id1);
        initialValues.put(KEY_ID2, id2);
        initialValues.put(KEY_TYPE, type);

        mDb.insert(DATABASE_TABLE_RELATION, null, initialValues);

        // Ajout de la relation inverse

        initialValues = new ContentValues();
        initialValues.put(KEY_ID1, id2);
        initialValues.put(KEY_ID2, id1);

        type=getTypeRelationInverse(type);

        initialValues.put(KEY_TYPE, type);
        return  mDb.insert(DATABASE_TABLE_RELATION, null, initialValues);
    }

    /**
     * Methode pour vérifier qu'une relation entre deux personne existe
     * @param : id1
     * @param :id2
     * @return boolean
     */
    public boolean existeRelation(long id1, long id2){
        Cursor mCursor = mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID, KEY_ID1, KEY_ID2, KEY_TYPE}, KEY_ID1 + "=" + id1 +" AND " + KEY_ID2 + "="+id2, null, null, null,null);
        if (null == mCursor || mCursor.getCount()  <1) {
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Methode pour récuperer les relations d'une personne
     */

    public Cursor getRelations(long rowId){
        return mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID, KEY_ID1, KEY_ID2, KEY_TYPE}, KEY_ID1 + "=" + rowId, null, null, null,null);
    }

    /**
     * Methode pour supprimer une relation
     */

    public boolean deleteRelation(long rowId){
        // Recuperation de la relation inverse
        Cursor relationCourante = mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID, KEY_ID1, KEY_ID2, KEY_TYPE}, KEY_ROWID + "=" + rowId, null, null, null,null);
        relationCourante.moveToFirst();
        long id2 = Long.parseLong(relationCourante.getString(relationCourante.getColumnIndexOrThrow(KEY_ID2)));
        long id1 = Long.parseLong(relationCourante.getString(relationCourante.getColumnIndexOrThrow(KEY_ID1)));
        Cursor relationInverse = mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID}, KEY_ID1 +"=" + id2 +" AND " + KEY_ID2 +"="+id1,null,null,null,null);
        relationInverse.moveToFirst();
        long idRelationInverse = Long.parseLong(relationInverse.getString(relationInverse.getColumnIndexOrThrow(KEY_ROWID)));
        return mDb.delete(DATABASE_TABLE_RELATION, KEY_ROWID + "=" + rowId, null) > 0 && mDb.delete(DATABASE_TABLE_RELATION, KEY_ROWID + "=" + idRelationInverse, null) > 0;
    }

    /**
     * Methode pour mettre a jour une relation
     */
    public boolean updateRelation(long rowId, long id1,long id2, long type){
        long idRelationInverse = getRelationInverse(rowId);
        //maj relation Courante
        ContentValues args = new ContentValues();
        args.put(KEY_ID1,id1);
        args.put(KEY_ID2, id2);
        args.put(KEY_TYPE, type);
        mDb.update(DATABASE_TABLE_RELATION, args, KEY_ROWID + "=" + rowId, null) ;

        //maj relation Inverse
        args = new ContentValues();
        args.put(KEY_ID1,id2);
        args.put(KEY_ID2, id1);
        args.put(KEY_TYPE, getTypeRelationInverse(type));
       return mDb.update(DATABASE_TABLE_RELATION, args, KEY_ROWID + "=" + idRelationInverse, null) >0 ;
    }

    /**
     * Methode pour retourner juste UNE relation
     * @param rowId
     * @return Cursor
     */
    public Cursor getCurrentRelation(long rowId){
        return mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID, KEY_ID1, KEY_ID2, KEY_TYPE}, KEY_ROWID + "=" + rowId, null, null, null,null);
    }

    public long getRelationInverse(long rowId){
        Cursor relationCourante = mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID, KEY_ID1, KEY_ID2, KEY_TYPE}, KEY_ROWID + "=" + rowId, null, null, null,null);
        relationCourante.moveToFirst();
        long id2 = Long.parseLong(relationCourante.getString(relationCourante.getColumnIndexOrThrow(KEY_ID2)));
        long id1 = Long.parseLong(relationCourante.getString(relationCourante.getColumnIndexOrThrow(KEY_ID1)));
        Cursor relationInverse = mDb.query(DATABASE_TABLE_RELATION, new String[] {KEY_ROWID}, KEY_ID1 +"=" + id2 +" AND " + KEY_ID2 +"="+id1,null,null,null,null);
        relationInverse.moveToFirst();

        return Long.parseLong(relationInverse.getString(relationInverse.getColumnIndexOrThrow(KEY_ROWID)));
    }

    public long getTypeRelationInverse(long type){
        if(type == 0){
            type =1;

        }else if(type == 1){
            type=0;
        }else{
            type=type;
        }
        return type;
    }

}

